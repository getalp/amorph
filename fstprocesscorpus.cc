// fstcompose.cc

// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//
// Copyright 2005-2010 Google, Inc.
// Author: riley@google.com (Michael Riley)
// Modified: jpr@google.com (Jake Ratkiewicz) to use FstClass
//
// \file
// Composes two FSTs.
//

#include <fst/script/compose.h>
#include <fst/script/project.h>
#include <fst/script/rmepsilon.h>
#include <fst/script/determinize.h>
#include <fst/script/minimize.h>
#include <fst/script/connect.h>
#include <fst/script/arcsort.h>

#include <dirent.h>
#include <sys/stat.h>
#include <sys/time.h>

DEFINE_string(compose_filter, "auto",
              "Composition filter, one of: \"alt_sequence\", \"auto\", "
              "\"match\", \"sequence\"");
DEFINE_bool(connect, true, "Trim output");

void processFile(std::string indir, std::string f, std::string outdir, const fst::script::FstClass *dfst, fst::ComposeOptions opts) {
  namespace s = fst::script;
  using fst::script::FstClass;
  using fst::script::MutableFstClass;
  using fst::script::VectorFstClass;
  
  struct stat statbuf;
  struct timeval start, end;
  long mtime, seconds, useconds;
  
  if(f == "." || f == "..")
    return;

  std::string o = outdir + "/" + f;

  if (! stat(o.c_str(),&statbuf)) {
    if (statbuf.st_size > 0) {
      cerr << "Ignoring already processed file: " << f << "\n";
      return;
    } 
  }
  
  gettimeofday(&start, NULL);
    
  // cerr << "Processing file: " << f ;
  FstClass *ifst1 = FstClass::Read(indir + "/" + f);
  if (!ifst1) {
    LOG(ERROR) << "Could not read file: " << f;
    return;
  }

  if (ifst1->ArcType() != dfst->ArcType()) {
    LOG(ERROR) << f << ": Input FSTs must have the same arc type.";
    return ;
  }
  
  VectorFstClass ofst(ifst1->ArcType());
  VectorFstClass mfst(ifst1->ArcType());

  s::Compose(*ifst1, *dfst, &ofst, opts);
  s::Project(&ofst, fst::PROJECT_OUTPUT);
  s::RmEpsilon(&ofst);
  s::Determinize(ofst, &mfst);
  s::Minimize(&mfst);
  s::ArcSort(&mfst, fst::script::OLABEL_COMPARE) ;
  
  mfst.Write(o);
  
  gettimeofday(&end, NULL);
  seconds = end.tv_sec - start.tv_sec;
  useconds = end.tv_usec - start.tv_usec;
  mtime = ((seconds) *1000 + useconds / 1000.0) + 0.5;
  cerr << "Processed file: " << o ;
  cerr << " [" << mtime << " ms]\n";
  
  return ;
}

int main(int argc, char **argv) {
  namespace s = fst::script;
  using fst::script::FstClass;
  using fst::script::MutableFstClass;
  using fst::script::VectorFstClass;

  string usage = "Composes all FST files in a dir with another FST.\n\n  Usage: ";
  usage += argv[0];
  usage += " dir1 in2.fst outdir\n";

  std::set_new_handler(FailedNewHandler);
  SetFlags(usage.c_str(), &argc, &argv, true);
  if (argc != 4) {
    ShowUsage();
    return 1;
  }

  string in1_dir = strcmp(argv[1], "-") != 0 ? argv[1] : "";
  string in2_name = (argc > 2 && (strcmp(argv[2], "-") != 0)) ? argv[2] : "";
  string out_dir = argc > 3 ? argv[3] : "";

  if (in1_dir.empty()) {
    LOG(ERROR) << argv[0] << ": Can't take standard input as its first arg.";
    return 1;
  }

  fst::ComposeFilter compose_filter;

  if (FLAGS_compose_filter == "alt_sequence") {
    compose_filter = fst::ALT_SEQUENCE_FILTER;
  } else if (FLAGS_compose_filter == "auto") {
    compose_filter = fst::AUTO_FILTER;
  } else if (FLAGS_compose_filter == "match") {
    compose_filter = fst::MATCH_FILTER;
  } else if (FLAGS_compose_filter == "sequence") {
    compose_filter = fst::SEQUENCE_FILTER;
  } else {
    LOG(ERROR) << argv[0] << "Unknown compose filter type: "
               << FLAGS_compose_filter;
    return 1;
  }

  fst::ComposeOptions opts(FLAGS_connect, compose_filter);

  DIR *dir;
  struct dirent *ent;
  
  dir = opendir (in1_dir.c_str());
  if (dir != NULL) {
  
  FstClass *ifst2 = FstClass::Read(in2_name);
  if (!ifst2) return 1;

    /* print all the files and directories within directory */
    while ((ent = readdir (dir)) != NULL) {

      processFile(in1_dir, ent->d_name, out_dir, ifst2, opts);

    }
    closedir (dir);
  } else {
    /* could not open directory */
    LOG(ERROR) << argv[0] << "first arg should be a readable directory.";
    return 1;
  }

}


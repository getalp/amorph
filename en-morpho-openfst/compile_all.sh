#!/bin/bash

for f in $1/*
do
  echo "Processing $f"
  iconv -c -f UTF-8 -t latin1 < $f | ./txt2fst.pl | fstcompile --isymbols=latin1.alpha --osymbols=latin1.alpha | \
fstcompose - downcase.fst > $2/`basename $f`
done
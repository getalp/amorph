#!/bin/bash


 ./txt2fst.pl | fstcompile --isymbols=latin1.alpha --osymbols=latin1.alpha | \
fstcompose - downcase.fst | fstcompose - lexicon_compact.fst | \
fstproject --project_output| fstrmepsilon | fstdeterminize | fstminimize | \
fstarcsort --sort_type=olabel | \
fstcompose  - dbpediaDevice.fst | fstproject --project_output | \
fstrmepsilon | fstdeterminize | fstminimize | fstarcsort --sort_type=olabel | \
fstcompose - dbpediaEukaryote.fst | fstproject --project_output | \
fstrmepsilon | fstdeterminize | fstminimize | fstarcsort --sort_type=olabel | \
fstcompose - dbpediaEvent.fst | fstproject --project_output | \
fstrmepsilon | fstdeterminize | fstminimize | fstarcsort --sort_type=olabel | \
fstcompose - dbpediaOrganisation.fst | fstproject --project_output | \
fstrmepsilon | fstdeterminize | fstminimize | fstarcsort --sort_type=olabel | \
fstcompose - dbpediaPerson.fst | fstproject --project_output | \
fstrmepsilon | fstdeterminize | fstminimize | fstarcsort --sort_type=olabel | \
fstcompose - dbpediaPlace.fst | fstproject --project_output | \
fstrmepsilon | fstdeterminize | fstminimize | fstarcsort --sort_type=olabel | \
fstcompose - dbpediaWork.fst | fstproject --project_output | \
fstrmepsilon | fstdeterminize | fstminimize | \
fstprint --isymbols=osymbs.alpha --osymbols=osymbs.alpha 

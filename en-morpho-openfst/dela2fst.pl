#!/usr/bin/perl -w
use utf8;
use strict;
use warnings;
use open qw(:std :utf8);

my $outfst = shift;
my $outsymbs = shift;

my $state_number=0;
#my %inchars;
my %outchars;
my %stopLemmas;

#initializeStopLemmas();

open (OUT,">:encoding(iso-latin-1)", $outfst);

my ($lemma, $form, $pos) = ();

while(<>) {
  if ($_ =~ /^ *<lemma>(.*)<\/lemma>/) {
    $lemma = $1;
  } elsif ($_ =~ /^ *<pos name='(.*)'\/>/) {
    $pos = convertpos($1);
  } elsif ($_ =~ /^ *<form>(.*)<\/form>/) {
    $form = $1;
    $form = cleanup($form);
    printpath($form, $pos, $lemma);
  } elsif ($_ =~ /^ *<\/entry>/) {
    ($lemma, $form, $pos) = ();
  }

#  if ($_ =~ /^([^\t]*)\t([^\t]*)\t([^\t]*)\t\[.*\]\t([^\t]*).*/) {
#    #print "$1\n";
#    printpath($1, $3, $4);
#  }
  
}


# The list of input char should be generated elsewhere and should not depend on the dictionary.
# open INALPHA, ">isymbs.txt";
# 
# print INALPHA "<eps> 0\n";
# my $charnum = 1;
# foreach my $key ( keys %inchars ) {
#   if ($key ne "<eps>") {
#     my $cn = $charnum++;
#     if ($key eq " ") {
#       print INALPHA "<space> $cn\n";
#     } else {
#       print INALPHA "$key $cn\n";
#     }
# 
#   }
# }
# close INALPHA;

open OUTALPHA, ">:encoding(iso-latin-1)", $outsymbs;

print OUTALPHA "<eps>\n";
print OUTALPHA "<space>\n";

my $charnum = 2;

# Add unknown characters to the output symbols list.
#for (my $i = 33; $i < 127; $i++) {
#  my $s = sprintf("%c", $i);
#  $s = "\\$s" if ($s eq "\"");
#  $s = "\\$s" if ($s eq "'");
#  my $cn = $charnum++;
#  print OUTALPHA "'$s'\n";
#}
#for (my $i = 160; $i < 256; $i++) {
#  my $s = sprintf("%c", $i);
#  my $cn = $charnum++;
#  print OUTALPHA "'$s'\n";
#}

my $key;
foreach $key ( keys %outchars ) {
  if ($key ne "<eps>") {
#    my $cn = $charnum++;
    print OUTALPHA "$key\n";
  }
}


close OUTALPHA;

sub printpath {
  my $occ = shift;
  my $pos = shift;
  my $lemma = shift;
  
  #print "$occ : $pos - $lemma\n";
  return if ($pos =~ /ponct.*/);
  return if ($pos =~ /parent.*/);
  return if ($pos =~ /epsilon/);
  return if ($stopLemmas{$lemma}); 

  my $o; my $in; my $out;
    
  $state_number++;
  
  printtrans(substr($occ, 0, 1), decodeLemma($lemma, $pos), 0, $state_number, 1.0);
  for(my $i = 1; $i < length($occ); $i++) {
    $o = $state_number++;
    $in = substr($occ, $i, 1);
    printtrans($in, "<eps>", $o, $state_number, 0);
  }
  print OUT "$state_number\n";
}

sub printtrans {
  my $in = shift;
  my $out = shift;
  my $f = shift;
  my $t = shift;
  my $w = shift;
  
  $out =~ s/ /_/g;
    
  if ($in eq " ") {
    $in = "<space>";
  } else {
    $in = "'$in'";
  }
  
  $outchars{$out} = 1;
  
  print OUT "$f $t $in $out\n";

}

sub cleanup {
  my $s = shift;
  $s =~ s/\\//g;
  $s =~ s/‚/é/;
  $s =~s/\x{201a}/'/g;
  $s =~s/\x{0192}/f/g;

  return $s;
}

sub decodeLemma {
  my $lemma = shift;
  my $pos = shift;

  $pos = convertpos($pos);
  $lemma = cleanup($lemma);

  return "#$lemma%$pos"
}

sub convertpos {
  my $pos = shift;
  
  if ($pos =~ /^n(.*)$/) {
    return "n";
  } elsif ($pos =~ /^adj(.*)$/) { 
    return "a";
  } elsif ($pos =~ /^verb(.*)$/) {
    return "v";
  } elsif ($pos =~ /^adv(.*)$/) {
    return "j";
  } elsif ($pos =~ /^GN$/) {
    return "n";
  } elsif ($pos =~ /^(.*)\.N$/) {
    return "n";
  } else {
    return $pos;
  }  
 }

sub initializeStopLemmas {
   $stopLemmas{"cln_____1"} = 1;
   $stopLemmas{"clr_____1"} = 1;
   $stopLemmas{"cld_____1"} = 1;
   $stopLemmas{"cla_____1"} = 1;
}
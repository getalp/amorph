// fstcompose.cc

// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//
// Copyright 2005-2010 Google, Inc.
// Author: riley@google.com (Michael Riley)
// Modified: jpr@google.com (Jake Ratkiewicz) to use FstClass
//
// \file
// Composes two FSTs.
//

#include <fst/script/compose.h>
#include <fst/script/project.h>
#include <fst/script/rmepsilon.h>
#include <fst/script/determinize.h>
#include <fst/script/minimize.h>
#include <fst/script/connect.h>
#include <fst/script/arcsort.h>

#include <dirent.h>
#include <sys/stat.h>
#include <sys/time.h>

DEFINE_string(compose_filter, "auto",
              "Composition filter, one of: \"alt_sequence\", \"auto\", "
              "\"match\", \"sequence\"");
DEFINE_bool(connect, true, "Trim output");

void processFile(std::string indir, std::string f, std::string outdir) {
  namespace s = fst::script;
  using fst::script::FstClass;
  using fst::script::MutableFstClass;
  using fst::script::VectorFstClass;
  
  struct stat statbuf;
  struct timeval start, end;
  long mtime, seconds, useconds;
  
  if(f == "." || f == "..")
    return;

  std::string o = outdir + "/" + f;

  if (! stat(o.c_str(),&statbuf)) {
    if (statbuf.st_size > 0) {
      cerr << "Ignoring already processed file: " << f << "\n";
      return;
    } 
  }
  
  gettimeofday(&start, NULL);
    
  // cerr << "Processing file: " << f ;
  VectorFstClass *ifst1 = VectorFstClass::Read(indir + "/" + f);
  if (!ifst1) {
    LOG(ERROR) << "Could not read file: " << f;
    return;
  }
  
  VectorFstClass mfst(ifst1->ArcType());

  s::Project(ifst1, fst::PROJECT_OUTPUT);
  s::RmEpsilon(ifst1);
  s::Determinize(*ifst1, &mfst);
  s::Minimize(&mfst);
  s::ArcSort(&mfst, fst::script::OLABEL_COMPARE) ;
  
  mfst.Write(o);
  
  gettimeofday(&end, NULL);
  seconds = end.tv_sec - start.tv_sec;
  useconds = end.tv_usec - start.tv_usec;
  mtime = ((seconds) *1000 + useconds / 1000.0) + 0.5;
  cerr << "Processed file: " << o ;
  cerr << " [" << mtime << " ms]\n";
  
  if (ifst1) delete ifst1;
  
  return ;
}

int main(int argc, char **argv) {
  namespace s = fst::script;
  using fst::script::FstClass;
  using fst::script::MutableFstClass;
  using fst::script::VectorFstClass;

  string usage = "remove epsilon, determinize minimize and arcsort all FST files in a dir.\n\n  Usage: ";
  usage += argv[0];
  usage += " indir outdir\n";

  std::set_new_handler(FailedNewHandler);
  SetFlags(usage.c_str(), &argc, &argv, true);
  if (argc != 3) {
    ShowUsage();
    return 1;
  }

  string in1_dir = strcmp(argv[1], "-") != 0 ? argv[1] : "";
  string out_dir = argc > 2 ? argv[2] : "";

  if (in1_dir.empty()) {
    LOG(ERROR) << argv[0] << ": Can't take standard input as its first arg.";
    return 1;
  }

  DIR *dir;
  struct dirent *ent;
  
  dir = opendir (in1_dir.c_str());
  if (dir != NULL) {
  
    /* print all the files and directories within directory */
    while ((ent = readdir (dir)) != NULL) {
      processFile(in1_dir, ent->d_name, out_dir);
    }
    closedir (dir);
  } else {
    /* could not open directory */
    LOG(ERROR) << argv[0] << "first arg should be a readable directory.";
    return 1;
  }

}


// fstprint.cc

// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//
// Copyright 2005-2010 Google, Inc.
// Author: riley@google.com (Michael Riley)
// Modified: jpr@google.com (Jake Ratkiewicz) to use FstClass
//
// \file
// Prints out binary FSTs in simple text format used by AT&T
// (see http://www.research.att.com/projects/mohri/fsm/doc4/fsm.5.html).

#include <fst/script/print.h>

#include <dirent.h>
#include <sys/stat.h>
#include <sys/time.h>

DEFINE_bool(acceptor, false, "Input in acceptor format");
DEFINE_string(isymbols, "", "Input label symbol table");
DEFINE_string(osymbols, "", "Output label symbol table");
DEFINE_string(ssymbols, "", "State label symbol table");
DEFINE_bool(numeric, false, "Print numeric labels");
DEFINE_string(save_isymbols, "", "Save input symbol table to file");
DEFINE_string(save_osymbols, "", "Save output symbol table to file");
DEFINE_bool(show_weight_one, false,
            "Print/draw arc weights and final weights equal to Weight::One()");
DEFINE_bool(allow_negative_labels, false,
            "Allow negative labels (not recommended; may cause conflicts)");

void printFile(std::string indir, std::string f, std::string outdir, const fst::SymbolTable *isyms, const fst::SymbolTable *osyms, const fst::SymbolTable *ssyms) {
  namespace s = fst::script;
  using fst::ostream;
  using fst::SymbolTable;
  
  struct timeval start, end;
  long mtime, seconds, useconds;
  
  if(f == "." || f == "..")
    return;

  std::string o = outdir + "/" + f;
  
  gettimeofday(&start, NULL);
    
  s::FstClass *fst = s::FstClass::Read(indir + "/" + f);
  if (!fst) {
    LOG(ERROR) << "Could not read file: " << f;
    return;
  }

  ostream *ostrm = &cout;
  ostrm = new fst::ofstream(o.c_str());
  if (!*ostrm) {
    LOG(ERROR) << "Could not open for writing, file = " << o;
    return;
  }
  ostrm->precision(9);

  s::PrintFst(*fst, *ostrm, o, isyms, osyms, ssyms,
              FLAGS_acceptor, FLAGS_show_weight_one);

  if (ostrm != &cout)
    delete ostrm;
  
  gettimeofday(&end, NULL);
  seconds = end.tv_sec - start.tv_sec;
  useconds = end.tv_usec - start.tv_usec;
  mtime = ((seconds) *1000 + useconds / 1000.0) + 0.5;
  cerr << "Processed file: " << o ;
  cerr << " [" << mtime << " ms]\n";
  
  return ;
}


int main(int argc, char **argv) {
  namespace s = fst::script;
  using fst::ostream;
  using fst::SymbolTable;

  string usage = "Prints out all binary FSTs from a dir in simple text format.\n\n  Usage: ";
  usage += argv[0];
  usage += " binary.fst.dir text.fst.dir\n";

  std::set_new_handler(FailedNewHandler);
  SET_FLAGS(usage.c_str(), &argc, &argv, true);
  if (argc != 3) {
    ShowUsage();
    return 1;
  }

  string in_dir = argv[1];
  string out_dir = argv[2];

  const SymbolTable *isyms = 0, *osyms = 0, *ssyms = 0;

  if (!FLAGS_isymbols.empty() && !FLAGS_numeric) {
    isyms = SymbolTable::ReadText(FLAGS_isymbols, FLAGS_allow_negative_labels);
    if (!isyms) exit(1);
  }

  if (!FLAGS_osymbols.empty() && !FLAGS_numeric) {
    osyms = SymbolTable::ReadText(FLAGS_osymbols, FLAGS_allow_negative_labels);
    if (!osyms) exit(1);
  }

  if (!FLAGS_ssymbols.empty() && !FLAGS_numeric) {
    ssyms = SymbolTable::ReadText(FLAGS_ssymbols);
    if (!ssyms) exit(1);
  }

  if (!isyms && !FLAGS_numeric) {
    LOG(ERROR) << argv[0] << ": Cannot print a whole directory without specifying isymbols ";
    exit(1);
  }
  if (!osyms && !FLAGS_numeric) {
    LOG(ERROR) << argv[0] << ": Cannot print a whole directory without specifying osymbols ";
    exit(1);
  }

  DIR *dir;
  struct dirent *ent;
  
  dir = opendir (in_dir.c_str());
  if (dir != NULL) {
  
      /* print all the files and directories within directory */
      while ((ent = readdir (dir)) != NULL) {

        printFile(in_dir, ent->d_name, out_dir, isyms, osyms, ssyms);

      }
      closedir (dir);
  } else {
    /* could not open directory */
    LOG(ERROR) << argv[0] << "first arg should be a readable directory.";
    exit(1);
  }


  return 0;
}

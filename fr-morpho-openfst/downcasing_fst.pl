#!/usr/bin/perl -w
# Author: Gilles Sérasset
# This script generates a fst that produces downcase alternatives to all upper case lettre in ISO latin 1.
#    This fst has only one state.
#    All transition loops by transducing x -> x and X -> {X, x}

use utf8;

my $i;

#TODO: downcase upper level characters.

#print "0 0 <eps> <eps>\n";
print "0 1 <space> <space>\n";
print "1 1 <space> <eps>\n";
print "1 0 <eps> <eps>\n";
for ($i = 33; $i < 127; $i++) {
  my $s = sprintf("%c", $i);
  if ($s =~/\p{Lu}/) {
    utf8::upgrade($s);
    my $l = lc($s);
    print("0 0 '$s' '$s'\n");
    print("0 0 '$s' '$l'\n");
  } else {
    $s = "\\$s" if ($s eq "\"");
    print("0 0 '$s' '$s'\n");
  }
}
# Include also the upper control char that may se used for œ or other non latin 1 chars.
for ($i = 128; $i < 256; $i++) {
  my $s = sprintf("%c", $i);
  if ($s =~/\p{Lu}/) {
    utf8::upgrade($s);
    my $l = lc($s);
    print("0 0 '$s' '$s'\n");
    print("0 0 '$s' '$l'\n");
  } else {
    print("0 0 '$s' '$s'\n");
  }
}

print("0\n");


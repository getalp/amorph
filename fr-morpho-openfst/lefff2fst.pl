#!/usr/bin/perl -w
use utf8;


my $state_number=0;
#my %inchars;
my %outchars;
#my %stopLemmas;

#initializeStopLemmas();

while(<>) {
  if ($_ =~ /^([^\t]*)\t([^\t]*)\t([^\t]*)\t\[.*\]\t([^\t]*).*/) {
    #print "$1\n";
    printpath($1, $3, $4);
  }
  
}

# The list of input char should be generated elsewhere and should not depend on the dictionary.
# open INALPHA, ">isymbs.txt";
# 
# print INALPHA "<eps> 0\n";
# my $charnum = 1;
# foreach my $key ( keys %inchars ) {
#   if ($key ne "<eps>") {
#     my $cn = $charnum++;
#     if ($key eq " ") {
#       print INALPHA "<space> $cn\n";
#     } else {
#       print INALPHA "$key $cn\n";
#     }
# 
#   }
# }
# close INALPHA;

open OUTALPHA, ">osymbs.txt";

print OUTALPHA "<eps> 0\n";
print OUTALPHA "<space> 1\n";

$charnum = 2;
foreach my $key ( keys %outchars ) {
  if ($key ne "<eps>") {
    my $cn = $charnum++;
    print OUTALPHA "$key $cn\n";
  }
}
# Add unknown characters to the output symbols list.
for ($i = 33; $i < 127; $i++) {
  my $s = sprintf("%c", $i);
  $s = "\\$s" if ($s eq "\"");
  my $cn = $charnum++;
  print OUTALPHA "'$s' $cn\n";
}
for ($i = 160; $i < 256; $i++) {
  my $s = sprintf("%c", $i);
  my $cn = $charnum++;
  print OUTALPHA "'$s' $cn\n";
}

close OUTALPHA;

sub printpath {
  my $occ = shift;
  my $pos = shift;
  my $lemma = shift;
  
  # print "$occ : $pos - $lemma\n";
  return if ($pos =~ /ponct.*/);
  return if ($pos =~ /parent.*/);
  return if ($pos =~ /epsilon/);
  return if ($stopLemmas{$lemma}); 

  my $o; my $in; my $out;
    
  $state_number++;
  
  printtrans(substr($occ, 0, 1), decodeLefffLemma($lemma, $pos), 0, $state_number, 1.0);
  for(my $i = 1; $i < length($occ); $i++) {
    $o = $state_number++;
    $in = substr($occ, $i, 1);
    printtrans($in, "<eps>", $o, $state_number, 0);
  }
  print "$state_number\n";
}

sub printtrans {
  my $in = shift;
  my $out = shift;
  my $f = shift;
  my $t = shift;
  my $w = shift;
  
  $out =~ s/ /_/g;
  if ($in eq " ") {
    $in = "<space>";
  }
  
#  $inchars{$in} = 1;
  $outchars{$out} = 1;
  print "$f $t $in $out $w\n";

}

sub decodeLefffLemma {
  my $lefffLemma = shift;
  my $pos = shift;

  $pos = convertPos($pos);
  
  if ($lefffLemma =~ /^(.*)_____\d$/) {
    return "#$1#$pos"
  } else { 
    return "#$lefffLemma#$pos"
  }
}

sub convertPos {
  my $pos = shift;
  
  if ($pos =~ /^n(.*)$/) {
    return "n"
  } elsif ($pos =~ /^adj(.*)$/) { 
    return "a"
  } elsif ($pos =~ /^adv(.*)$/) {
    return "j"
  } else {
    return $pos;
  }  
 }

sub initializeStopLemmas {
   $stopLemmas{"cln_____1"} = 1;
   $stopLemmas{"clr_____1"} = 1;
   $stopLemmas{"cld_____1"} = 1;
   $stopLemmas{"cla_____1"} = 1;
}
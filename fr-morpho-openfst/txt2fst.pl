#!/usr/bin/perl -w

use utf8;
my $state_number = 0;
my $line;

while (<>) {
  $line = $_;
  $line =~s/\n//g;
  $l = length($line);
  for(my $i = 0; $i < $l; $i++) {
    $o = $state_number++;
    $in = substr($line, $i, 1);
    printtrans($in, $in, $o, $state_number);
  }
  $o = $state_number++;
  $in = " ";
  printtrans($in, $in, $o, $state_number);
}
print "$state_number\n";

sub printtrans {
  my $in = shift;
  my $out = shift;
  my $f = shift;
  my $t = shift;
  
  if (ord($in) < 32 or $in eq " ") {
    $in = "<space>"; 
  }  else {
    $in = "'$in'";
  }
  if (ord($out) < 32 or $out eq " ") {
    $out = "<space>";
  } else {
    $out = "'$out'";
  }
  $in = "'\\\"'" if ($in eq "'\"'");
#  $in = "'\\''" if ($in eq "'''");
  $out = "'\\\"'" if ($out eq "'\"'");
#  $out = "'\\''" if ($out eq "'''");

  print "$f $t $in $out\n";

}
#!/usr/bin/perl -w
# Author: Gilles Sérasset
# This script generates a fst that produces downcase alternatives to all upper case lettre in ISO latin 1.
#    This fst has only one state.
#    All transition loops by transducing x -> x and X -> {X, x}

use utf8;
use strict;
binmode(STDERR, ":utf8");

use Getopt::Std;

our %opts;
usage() if (! getopts('i:o:t:', \%opts));

my $DEFAULT_OSYMBS = "osymbs.txt";

my $osymb = $opts{'o'} || $DEFAULT_OSYMBS;
my $typefile = $opts{'i'};
my $type =  $opts{'t'};;

if ($type) {
  die "-i option is mandatory if -t option is given.\n" if (! $typefile);
  if ($type =~ m|^(?!http://)|) {
    $type = "http://dbpedia.org/ontology/$type";
  }
}

my %labels;
my $state_number=0;
my %outchars;

loadInstanceFilter();
processDbpediaNqFile();
appendNewOutputSymbols();

sub usage {
  printf STDERR "USAGE: $0 [-o osymbs.txt] [-i instancetypefile -t uri]\n";
  printf STDERR "       -o : specify the file name where the list of output symbols will be stored (default: osymbs.txt).\n";
  printf STDERR "       -i : specify the file name containing the instance type of dbpedia (mandatory if -t is specified).\n";
  printf STDERR "       -t : specify the uri of the type of instances to be included in the fst (defalut: include everything).\n";
  printf STDERR "            if the uri does not begin with http://, it will be prefixed by \"http://dbpedia.org/ontology/\" \n";
  exit;
}

sub loadInstanceFilter {
  return if (! $type);
  open TF, "$typefile" || die "Could not open $typefile";

  # printf STDERR "Searching $type...";
  while (<TF>) {
    if ($_ =~ m|^<([^>]*)> <[^>]*> <$type>|) {      
      $labels{$1}=1;
    } else {
      # printf STDERR "ignoring entry : $_";
    }
  }
}

sub processDbpediaNqFile {
  #printf STDERR "Processing file...\n";
  while(<>) {
    # We are in a dbpedia label file
    if ($_ =~ m|^<([^>]*)> <http://www.w3.org/2000/01/rdf-schema#label> "(.*)"\@\w\w.*|) {      
      printpath($2, $1) if ($labels{$1} || ! $type);
    } elsif ($_ =~ m|^<([^>]*)> <http://lexvo.org/ontology#label> "(.*)"\@\w\w .*|) {
      printpath($2, $1) if ($labels{$1} || ! $type);
    } else {
      warn "Malformed line: $_";
    }
  }
}

sub printpath {
  my $occ = shift;
  my $uri = shift;
  
  $occ = normalizeLabel($occ);
  return if $occ =~ /[^\x00-\xFF]/; # Do not use labels with non latin 1 chars.
  return if $occ eq "";
  
  $uri = normalizeUri($uri);
  
  
  my $o; my $in; my $out;
    
  $state_number++;
  
  printtrans(substr($occ, 0, 1), $uri, 0, $state_number, "");
  for(my $i = 1; $i < length($occ); $i++) {
    $o = $state_number++;
    $in = substr($occ, $i, 1);
    printtrans($in, "<eps>", $o, $state_number, "");
  }
  print "$state_number\n";
}

sub printtrans {
  my $in = shift;
  my $out = shift;
  my $f = shift;
  my $t = shift;
  my $w = shift;
  
  $out =~ s/ /_/g;
  if ($in eq " ") {
    $in = "<space>";
  } elsif ($in eq '"') {
    $in = "'\\\"'"
  } else {
    $in = "'$in'";
  }
  
  $outchars{$out} = 1;
  print "$f $t $in $out $w\n";

}

sub normalizeUri {
  my $uri = shift;
  
  if ($uri =~ m|^http://dbpedia\.org/resource/(.*)$|) {
    return "%$1"
  } else { 
    warn "Malfored URI: $uri";
    return "";
  }
}

sub normalizeLabel {
  my $lbl = shift;
  
  # suppress everything that is inside perens as it is usually a disambiguation string.
  $lbl =~ s/\([^\)]*\)\s*$//g;

  return "" if ($lbl =~ /\\U/);

  $lbl = trim($lbl);
  
  $lbl =~ s/\\u([A-Fa-f0-9]{4})/chr(hex($1))/eg;  
  
  $lbl =~ s/\x{2013}/-/g;
  $lbl =~ s/\x{2019}/'/g;

  #TODO interpret \uXXXX sequences.
  # \u00xx are latin1 one chars
  # \u2013 should be replaced by -
  # \u2019 should be replaced by '

  #print STDERR "$lbl\n" if ($lbl =~ /\\u/);
  #print STDERR "$lbl\n" if ($lbl =~ /\(/);

  return trim($lbl);
}

sub appendNewOutputSymbols {  
  
  open OUTALPHA, ">$osymb" || die "Could not append output symbols to alphabet $osymb";
  
  foreach my $key ( keys %outchars ) {
    if ($key ne "<eps>") {
      print OUTALPHA "$key\n";
    }
  }
}


sub trim {
  my $str = shift;
  $str =~s/^\s+//g;
  $str =~s/\s+$//g;
  $str =~s/\s+/ /g;
  
  return $str;
}

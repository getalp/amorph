#!/usr/bin/perl -w
# Author: Gilles Sérasset
# This script generates a fst that produces downcase alternatives to all upper case lettre in ISO latin 1.
#    This fst has only one state.
#    All transition loops by transducing x -> x and X -> {X, x}

use utf8;

my $i;

#TODO: downcase upper level characters.

print "<eps>\n";
print "<space>\n";
for ($i = 33; $i < 127; $i++) {
  my $s = sprintf("%c", $i);
  $s = "\\$s" if ($s eq "\"");
#  $s = "\\$s" if ($s eq "'");
  print ("'$s'\n");
}

# Include also the upper control char that may se used for œ or other non latin 1 chars.
for ($i = 128; $i < 256; $i++) {
  my $s = sprintf("%c", $i);
  print("'$s'\n");
}



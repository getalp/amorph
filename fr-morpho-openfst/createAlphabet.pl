#!/usr/bin/perl -w
# Author: Gilles Sérasset
# This script generates a fst that produces downcase alternatives to all upper case lettre in ISO latin 1.
#    This fst has only one state.
#    All transition loops by transducing x -> x and X -> {X, x}

use utf8;
use strict;

use Getopt::Std;

our %opts;
usage() if (! getopts('l', \%opts));

my $latin1only = $opts{'l'};

#print STDERR "Latin1 only" if $latin1only;

my $sid = 0;
my %symbols;

addsymbol('<eps>');

foreach my $arg (@ARGV) {
  processSymbols($arg);
}

sub usage {
  print STDERR "USAGE: $0 [-l] file1 file2 ...\n";
  print STDERR "  Merge all symbols in given files and produces an alphabet file suitable for openfst. \n";
  printf STDERR "       -l : Only produce latin1 symbols.\n";
  exit;
}

sub processSymbols {
  my $fname = shift;
  
  open SF, "$fname" || die "Could not open $fname";

  while (<SF>) {
    chomp;
    addsymbol($_);
  }
}

sub addsymbol {
  my $s = shift;
  my $i = $sid++;
  
  
  # print STDERR "Duplicate symbol: $s\n" if ($symbols{$s});
  return if ($symbols{$s});
  if ($latin1only) {
      return unless ($s =~/^'.*'$/ || $s eq "<space>" || $s eq '<eps>');
  }
  $symbols{$s} = 1;
  print "$s $i\n";
}

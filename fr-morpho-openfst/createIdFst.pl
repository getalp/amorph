#!/usr/bin/perl -w
# Author: Gilles Sérasset
# This script generates a fst that gives an id transduction on all symbols, except punctuation ones.
#    This fst has only one state.
#    All transition loops by transducing x -> x unless x is a punctuation

use utf8;
use strict;

use Getopt::Std;

my %symbols;
$symbols{'<eps>'} = 1;

foreach my $arg (@ARGV) {
  processSymbols($arg);
}

print "1\n";

sub usage {
  print STDERR "USAGE: $0 file1 file2 ...\n";
  print STDERR "  Creates an id fst on the symbols contained in argument files, ignoring puntuation symbol. \n";
  exit;
}

sub processSymbols {
  my $fname = shift;
  
  open SF, "$fname" || die "Could not open $fname";

  while (<SF>) {
    chomp;
    addsymbol($_);
  }
}

sub addsymbol {
  my $s = shift;
  
  if ($s =~ /'(.*)'/) {
    return if ($1 =~/\p{P}/);
  }
  print STDERR "Duplicate symbol: $s\n" if ($symbols{$s});
  return if ($symbols{$s});
  
  $symbols{$s} = 1;
  print "0 1 $s $s\n";
}

#!/usr/bin/perl -w
# Author: Gilles Sérasset
# This script generates a fst that transduce all symbols in ISO latin 1.
#    This fst has only one state.
#    All transition loops by transducing x -> 'x'

use utf8;

my $i;

#print "0 0 <eps> <eps>\n";
#print "0 1 <space> <space>\n";
for ($i = 33; $i < 127; $i++) {
  my $s = sprintf("%c", $i);
  next if ($s =~/\p{P}/);
  print("0 1 '$s' '$s'\n");
}
for ($i = 160; $i < 256; $i++) {
  my $s = sprintf("%c", $i);
  next if ($s =~/\p{P}/);
  print("0 1 '$s' '$s'\n");
}

print("1\n");

